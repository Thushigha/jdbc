package bcas.adp.dbs;

import java.sql.SQLException;

public interface DataManipulation {

	public void insertData(String name, int age) throws SQLException;
	
	public void readData() throws SQLException;
	
	public void readData(String searchText) throws SQLException;
	
	public void updateData(String name, int age ) throws SQLException;
	
	public void deleteData(String name) throws SQLException;
}
