package bcas.ap.dbs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCSingleton {
	
	//Create a JDBCSingleton class
	//static member holds only one instance of the JDBCSingleton class
	private static JDBCSingleton jdbc;
	
	//JDBCSingleton prevent the instantiation from any other class
	private JDBCSingleton() {
	}
	
	//Here we provides global point of access
	public static JDBCSingleton getInstance() {
		
		if(jdbc==null) {
			jdbc=new JDBCSingleton();
		}
		return jdbc;
	}
	
	//to get connection from methods like insert data/view data etc
	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection connection = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection("JDBC:mysql://localhost:3306/csd16","root","root");
		return connection;
	}
	
	//to insert the record into the database
	public void insertData(String name, int age) throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		
		
		try {
			connection=this.getConnection();
			//statement=connection.prepareStatement("INSERT INTO student (id,name) VALUES('2','Nivetha')");
			//statement.executeUpdate();
			statement = connection.prepareStatement("INSERT INTO student (name,age) VALUES(?,?)");
			statement.setString(1, name);
			statement.setInt(2, age);
			statement.executeUpdate();
			
			
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		} finally {
			if (connection != null ) {
				connection.close();
			}
			if (statement != null ) {
				statement.close();
			}
		}
		
	}
	
	// to read the Record from the Database 
		public void readData() throws SQLException {
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet=null;
			
			try {
				connection=this.getConnection();
				statement=connection.prepareStatement("SELECT * FROM student");
				resultSet = statement.executeQuery();
				System.out.println("ID\tName\tAge");
				while (resultSet.next()) {
				System.out.println(resultSet.getString(1) + "\t" + resultSet.getString(2) + "\t" + resultSet.getString(3));
				}
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} finally {
				if(resultSet != null)
					resultSet.close();
				if (connection != null ) {
					connection.close();
				}
				if (statement != null ) {
					statement.close();
				}
			}
		}
	
	// to update the Record from the Database with search parameter
	public void readData(String searchText) throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet=null;
		
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("SELECT * FROM student WHERE name=?");
			statement.setString(1, searchText);
			resultSet = statement.executeQuery();
			System.out.println("ID\tName");
			while (resultSet.next()) {
			System.out.println(resultSet.getString(1) + "\t" + resultSet.getString(2));
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if(resultSet != null)
				resultSet.close();
			if (connection != null ) {
				connection.close();
			}
			if (statement != null ) {
				statement.close();
			}
		}
	}

	// to update the record from the database with parameter
	public void updateData(String name, int age ) throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		
		try {
			connection = this.getConnection();
			statement = connection.prepareStatement("UPDATE student SET age=? WHERE name=?");
			statement.setInt(1, age);
			statement.setString(2, name);
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null ) {
			connection.close();
		}
			if (statement != null ) {
			statement.close();
		}
	}
	}
	
	// to delete the record from the database
		public void deleteData(String name) throws SQLException {
			Connection connection = null;
			PreparedStatement statement = null;
			
			try {
				connection = this.getConnection();
				statement = connection.prepareStatement("DELETE FROM student WHERE name=?");
				statement.setString(1, name);
				statement.executeUpdate();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} finally {
				if (connection != null ) {
				connection.close();
			}
				if (statement != null ) {
				statement.close();
			}
		}
		}

	
}
 

