package bcas.ap.dbs;

import java.sql.SQLException;

public class DBDemo {

	public static void main(String[] args) {
		JDBCSingleton jdbc = JDBCSingleton.getInstance();
		//System.out.println("Data inserted");
		try {
			//jdbc.insertData("3", "Akalya");
			//jdbc.readData();
			//jdbc.readData("Nivetha");
			//jdbc.updateData("Nivetha", 25);
			jdbc.readData();
			//jdbc.insertData("Sumi",21);
			//System.out.println("Data inserted");
			jdbc.deleteData("Sumi");
			jdbc.readData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
