package bcas.ap.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

	public static void main(String[] args) {
		System.out.println("........... My SQL JDBC Connection Test ........");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("Attached the JDBC Driver");
		}
		catch(ClassNotFoundException e) {
			System.err.println("Fail to Attached the JDBC Driver");
			e.printStackTrace();
		}
		
		Connection connection = null;
		try {
			connection =DriverManager.getConnection("JDBC:mysql://localhost:3306/csd16","root","root");
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		if (connection != null)
		{
			System.out.println("Successfully connected Database");
		}
		else
		{
			System.out.println("Connection Fail");
		}
		
	}

}
